#ifndef COMMON_H_
#define COMMON_H_


#include <assert.h>
#include "logger.h"
#include "exception.h"
#include "message.h"

#include <vector>
#include <string>
#include <map>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp> 
#include <boost/shared_ptr.hpp>

using std::string;
using std::stringstream;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::map;
using boost::posix_time::ptime;
using boost::shared_ptr;

#endif

