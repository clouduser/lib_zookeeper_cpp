#include <stdio.h>
#include <iostream>
#include <string.h>
#include <zookeeper.h>

#include <zk_lib/zk_acl.h>

/*
 * we need step 1~4 to complete zk acl
 * step1: zookeeper_init to get zhandle
 * step2: init acl to get acl_vec
 * step3: addauth to get permision of zk operation
 * step4: use digest when create /zknode
 */

using namespace bizbase::zkacl;

int main() 
{
	std::string user = "user_admin";
	std::string pass = "pass_admin";
	struct ACL_vector acl_vec; 

	//step (1)
	zhandle_t *zk = zookeeper_init("127.0.0.1:2181", NULL, 10000, 0, NULL, 0);

	//step (2)
	bool ret = InitAcl(user, pass, acl_vec);
	if(ret) {
		printf("initacl success\n");
		//step (3)
		ret = AddAuth(zk, user, pass);
	}

	if(! ret) {
		printf("addauth fail\n");
		return 0;
	}

	printf("addauth success\n");


	//step (4)
	char buffer[512];
	int rc = zoo_create(zk, "/zknode", "bizbase", 7, &acl_vec, 0, buffer, sizeof(buffer)-1);
	if(rc) {
		printf("create fail, ret:%d\n", rc);
		return 0;
	}

	printf("create /zknode success\n");



	memset(buffer,0,sizeof(buffer));
	int buflen= sizeof(buffer);
	struct Stat stat;
	rc = zoo_get(zk, "/zknode", 0, buffer, &buflen, &stat);
	if(rc) {
		fprintf(stderr, "zoo_get error %d for %d\n", rc, __LINE__);
	}
	printf("get /zknode, buffer: %s\n", buffer);

	getchar();

	rc = zoo_delete(zk, "/zknode", -1);
	if(rc) {
		fprintf(stderr, "zoo_delete error %d for %d\n", rc, __LINE__);
	}

	return 0;
}


/*
   structACL_vector {

   int32_t count;

   struct ACL *data;

   };
   struct ACL {

   int32_tperms;

   struct Idid;

   };
   struct Id {

   char *scheme;

   char * id;

   };
   */
