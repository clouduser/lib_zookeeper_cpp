#include <stdio.h>
#include <iostream>
#include <string.h>
#include <zookeeper.h>

#include <zk_lib/zk_lock.h>

using namespace bizbase::zklock;
using namespace bizbase::zkclient;


void CallBack(Event ev) 
{
	printf("==================callback\n");
	if(ev._state != ZOO_CONNECTED_STATE) {
	}
}

int main() 
{
	Biz::Logger().config("test.prop");
	int i=0;
	while(1){

		try {
			boost::shared_ptr<ZkClient> zkclient_ptr = boost::shared_ptr<ZkClient>(new ZkClient(
						"127.0.0.1:2181", 
						300));

			boost::shared_ptr<ZkLock> zklock_ptr = boost::shared_ptr<ZkLock>(new ZkLock(
						zkclient_ptr,
						"/lock_path",
						"lock_data",
						"content"));

			zkclient_ptr->CreateRecursion("/a/b", "content");

			zkclient_ptr->SetEventHandler(boost::bind(CallBack, _1));

			int ret = zklock_ptr->Lock();
			if(0 != ret) {
				zklock_ptr->Unlock();
				printf("lock failed\n");
			} else {
				printf("lock success\n");
			}

			int i=0;
			while(zklock_ptr->IsLocked()) {
				printf("===============locked, i:%d\n", i++);
				sleep(1);
			}
			/*
			
			*/
			std::cout << "UNLOCK==================" << i << std::endl;

			zklock_ptr->Unlock();

		} catch(BizException& ex) {
			printf("error:%s\n", ex.what());
		}

		sleep(1);
		std::cout << "==================" << i++ << std::endl;
	}

	return 0;
}
